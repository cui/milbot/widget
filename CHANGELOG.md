## [2.0.1](https://gitlab.unige.ch/cui/milbot/widget/compare/2.0.0...2.0.1) (2023-11-21)


### Bug Fixes

* **config:** remove action server params ([13c29d1](https://gitlab.unige.ch/cui/milbot/widget/commit/13c29d10935414ad8f0bfcd14d9a5867d4c6e2a0))
* **js:** update the custom action request ([6149c40](https://gitlab.unige.ch/cui/milbot/widget/commit/6149c40751d1abc7f448e67d0afb6238ea53901a))


### General maintenance

* **gitignore:** add config.json file ([6bb6408](https://gitlab.unige.ch/cui/milbot/widget/commit/6bb64082c9c3ab99622dd86d99a2982432cea2c8))


### Refactoring

* **js:** replace console.log with console.debug ([8771220](https://gitlab.unige.ch/cui/milbot/widget/commit/8771220df42d8767b9aead37045ba6b8b2e457e2))

## [2.0.0](https://gitlab.unige.ch/cui/milbot/widget/compare/1.3.2...2.0.0) (2023-11-14)


### ⚠ BREAKING CHANGES

* **config:** add action_server param

### Features

* **config:** add action_server param ([73b5fb9](https://gitlab.unige.ch/cui/milbot/widget/commit/73b5fb94b2e59ed80e52c1a2e51343e994544264))


### General maintenance

* **readme:** update config params list ([98d7ed2](https://gitlab.unige.ch/cui/milbot/widget/commit/98d7ed20cd1a689d7002fe2667140d36ccc8aa60))

## [1.3.2](https://gitlab.unige.ch/cui/milbot/widget/compare/1.3.1...1.3.2) (2023-11-13)


### Bug Fixes

* **config:** add functions for params ([90c6ff5](https://gitlab.unige.ch/cui/milbot/widget/commit/90c6ff587ec728192619fd461446582522005e18))

## [1.3.1](https://gitlab.unige.ch/cui/milbot/widget/compare/1.3.0...1.3.1) (2023-11-13)


### Bug Fixes

* **config:** typo in default values ([255877b](https://gitlab.unige.ch/cui/milbot/widget/commit/255877bb655aa03b28dd9ca97af8fa6866c272a6))

## [1.3.0](https://gitlab.unige.ch/cui/milbot/widget/compare/1.2.0...1.3.0) (2023-11-13)


### Features

* **config:** add default config params and config.json file ([b424899](https://gitlab.unige.ch/cui/milbot/widget/commit/b424899d46e74147d1d7c4beb51977acf8a8c3ac))


### General maintenance

* **readme:** add instructions for running the widget ([ebb3407](https://gitlab.unige.ch/cui/milbot/widget/commit/ebb34075b5446a10b3e53d1d5f6deba17ecbd6ef))

## [1.2.0](https://gitlab.unige.ch/cui/milbot/widget/compare/1.1.0...1.2.0) (2023-11-07)


### Features

* **gulp:** add html minifier ([3c821e3](https://gitlab.unige.ch/cui/milbot/widget/commit/3c821e3985eaa2bc759c279ee35f22ecdf0a448f))
* **gulp:** add sass minifier ([d08d1ea](https://gitlab.unige.ch/cui/milbot/widget/commit/d08d1ea7a42aab1a9fa39681361a9085e63cf003))


### General maintenance

* **release:** 1.2.0 [skip ci] ([6d76570](https://gitlab.unige.ch/cui/milbot/widget/commit/6d76570a22590f8dac90acd25d101528183055d2))

## [1.2.0](https://gitlab.unige.ch/cui/milbot/widget/compare/1.1.0...1.2.0) (2023-11-07)


### Features

* **gulp:** add html minifier ([3c821e3](https://gitlab.unige.ch/cui/milbot/widget/commit/3c821e3985eaa2bc759c279ee35f22ecdf0a448f))
* **gulp:** add sass minifier ([d08d1ea](https://gitlab.unige.ch/cui/milbot/widget/commit/d08d1ea7a42aab1a9fa39681361a9085e63cf003))

## [1.1.0](https://gitlab.unige.ch/cui/milbot/widget/compare/1.0.0...1.1.0) (2023-11-07)


### Features

* **stylesheet:** translate css to scss ([7d05d37](https://gitlab.unige.ch/cui/milbot/widget/commit/7d05d37c4741290ebdc6305708caf309a0e5ffa7))


### Build and continuous integration

* **gulp:** add scss compilation ([c9f22cd](https://gitlab.unige.ch/cui/milbot/widget/commit/c9f22cd9f4b79995271e4858927d8050be88fe05))

## 1.0.0 (2023-11-03)


### Bug Fixes

* **stylesheet:** code formatting and minor issues ([363060c](https://gitlab.unige.ch/cui/milbot/widget/commit/363060c2729c010ec832817d493cddb6f1be67bc))


### Build and continuous integration

* add config ([01c2dd1](https://gitlab.unige.ch/cui/milbot/widget/commit/01c2dd1519fe32427f55a5c324ecd5faf82108ae))
* add semantic-release config ([30d805b](https://gitlab.unige.ch/cui/milbot/widget/commit/30d805b24140066d590ffb431805f9ccab4b996c))


### General maintenance

* add gitignore ([89df3e1](https://gitlab.unige.ch/cui/milbot/widget/commit/89df3e1cc27903bc83bc784fbc45cb448b8864e5))
* **docker:** add dockerfile ([67cc349](https://gitlab.unige.ch/cui/milbot/widget/commit/67cc349d18bd605760f627dafff333d9a363b403))
* **gulp:** add gulp config ([80b5836](https://gitlab.unige.ch/cui/milbot/widget/commit/80b5836d3e85ebb60cf73fbce9aa52222bb9f072))
* import files from deployed version ([e8ebd74](https://gitlab.unige.ch/cui/milbot/widget/commit/e8ebd74656ddc024f8abd6eb0c6d8542f1c972f3))
* import project ([9ea9a53](https://gitlab.unige.ch/cui/milbot/widget/commit/9ea9a53a0a3ef034f52afc1a0da9084f9ada2028))
* import style.css ([997d583](https://gitlab.unige.ch/cui/milbot/widget/commit/997d583fbabe797537d8c88e336635feba3edc91))


### Refactoring

* **js:** major code refactor ([b78324c](https://gitlab.unige.ch/cui/milbot/widget/commit/b78324cd3668088c186f2620e31a2b3346564906))
* move code to src folder ([8ceda5a](https://gitlab.unige.ch/cui/milbot/widget/commit/8ceda5abec638f025ff88f7c746977a69e40bfc4))
