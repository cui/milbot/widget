const { src, dest, series} = require('gulp');
const uglify = require('gulp-uglify');
const sass = require('gulp-sass')(require('sass'));
const htmlmin = require('gulp-htmlmin');

function html() {
    return src('src/*.html')
        .pipe(htmlmin({
            collapseWhitespace: true,
            removeComments: true
        }))
        .pipe(dest('dist/'));
}

function css() {
    return src('src/**/*.css')
        .pipe(dest('dist/'));
}

function scss() {
    return src('src/**/style.scss', {base: 'src/static/sass/'})
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(dest('dist/static/css'));
}

function js() {
    return src(['src/**/*.js'])
        .pipe(uglify())
        .pipe(dest('dist/'));
}

function img() {
    return src(['src/**/*_compressed.gif', 'src/**/*_small.gif', 'src/**/img/**'])
        .pipe(dest('dist/'));
}

exports.html = html;
exports.css = css;
exports.scss = scss;
exports.js = js;
exports.img = img;

exports.default = series(css, scss, js, img, html);
