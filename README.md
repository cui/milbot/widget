# Milbot Widget

The docker image is using the following default hard-coded config parameters
- `ACTION_NAME`
- `RASA_SERVER`


## Deployment

### Default
> docker run -p 8080:80 registry.gitlab.unige.ch/cui/milbot/widget

### Custom config
If you need to change the default params you can pass a `config.json` file. You can use the [`config.default.json`](https://gitlab.unige.ch/cui/milbot/widget/-/blob/master/src/config.default.json) file as template.
> docker run -v $(pwd)/config.json:/usr/share/nginx/html/config.json:ro -p 8080:80 registry.gitlab.unige.ch/cui/milbot/widget
