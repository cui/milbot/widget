let ACTION_NAME = "action_bonjour";
let RASA_SERVER = "http://milbot-core:5005";
const SENDER_ID = uuidv4();

fetch('config.json')
    .then(response => {
        if (!response.ok) {
            throw new Error('No config.json file was found. Default values will be used');
        }
        return response.json();
    })
    .then(config => {
        ACTION_NAME = config.ACTION_NAME;
        RASA_SERVER = config.RASA_SERVER;
    })
    .catch(error => {
        console.warn('Config not found', error);
    });

let config = {
    action_name: function () {
        return ACTION_NAME;
    },
    server_url: function () {
        return RASA_SERVER;
    },
    sender_id: function () {
        return SENDER_ID;
    }
};
